* `,eb` - evaluate entire buffer
* `,ee` - evaluate the form we're on.
* `,er` - evaluate the outermost form under our cursor
* `,e!` - evaluate the form and replace it with the result
* `,em<mark>` - evaluate the form at `<mark>`. set marks with `m`. Hitting the capital of the mark opens it even if it's in another file.
* `,ew` - inspect the below variable
* `,E` - evaluate the current visual selection
* `,E<motion>` - evaluate the code covered by the motion.
* `,Ea(` - not totally clear? evaluate the form you're on, not sure how it's different from `,ee`.
* `,l<svt>` - open a log buffer in a horizontal split, vertical split, or tab respectively
