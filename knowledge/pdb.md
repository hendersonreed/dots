# Conditional breakpoints

`break filename:<linenumber|function-name>, conditional`

like so:

`break iqe-automation-hub-plugin/tests/api/test_artifact_upload.py:test_ansible_lint_exception, x == 15`

will only break when x == 15
