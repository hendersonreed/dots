# thoughts on design:

I was reading the front page of http://halestrom.net/darksleep/software/minisleep/ and was immediately enchanted with how it is designed.

The key highlights:

- Several options to write pages
    - Online interface: HTML WYSIWYG with drag- and- drop image support (no separate uploading necessary).  Recommended for new users.
    - Supports any markup to HTML converter of your choice:  markdown, textile, mediawiki, bbcode, reStruturedText, uuencoded RTF, etc.  Adding your own requires adding one line of code to build_page.sh
    - Pages can be edited locally (with your favourite editor) and synced to your site using tools like git, rsync or unison. No additional complexity or config required.
    - Optional feature (disabled by default): a page can be an executable script, where any text printed becomes the page.  Useful for generating site indexes, such as front pages for blogs.
- Lightweight: Almost zero dependencies
    - Common commandline utilities and a HTTP webserver that supports CGI (eg apache, lighttpd, hiawatha, yaws)
    - Ridiculously easy to setup and to move from host to host.
    - No daemon, no database.
- Less than 1000 lines of code: designed to be understandable and fixable by non- experts.
    - Statically compiles pages: fast & resilient to failure
    - All scripts can be disabled or fail and the site will still stay online.  You can repair problems in your own time.
- Likely faster page delivery than any traditional dynamic wiki software (Mediawiki, Doku, etc).
- Dumb and simple security
    - HTTP authentication, handled by your webserver instead of this wiki's scripts.
    - Documentation & examples are provided for several popular webservers.
    - No notable attack surface if you keep your login credentials safe.
    - Anti- CSRF tokens.
- No database: pages stored as files and folders
    - Dead simple to administrate and backup.
    - Very easy to migrate page content to and from other wikis and systems, so you don't feel trapped.
- Easy to theme
    - Comes with one very short CSS file, rather than one filled with dark magic.
    - One short script acts as the page template, intended to be edited & kept by users across updates.

I think you could make the argument that I should just USE it as it is. However, I want to make my own alternative, because I want to make it in Racket and I want a project to work on, not just a wiki to make.

## Important features of mine:

1. use a custom-written web server, rather than a specially configured standard one.
    - the primary responsibility would be to show the compiled pages.
    - additionally, it would expose a small and simple API to allow editing of pages using a WYSIWYG edit page.
        - this is actually hugely important ime and should be an early requirement.
2. Several options to write pages:
    - would like the same WYSIWYG page editor, if possible.
    - support any markup to html converter
         - hmmmm nope.
    - local page editing
    - optional feature: executable pages, where printed text becomes the page.
         - this is the way, everything will be built this way. Provide an entrypoint script, and then you can use any converter you want.
3. fewest possible installed dependencies:
    - I'd like to use as few as possible Racket dependencies.
    - there probably needs to be some kind of external dependency for the WYSIWYG page editor. EDIT: actually, we _might_ be able to port the one from minisleep to fit our needs. EDIT 2: there is enough in HTML and JS that we don't need this, we can build our own.
4. HTTP authentication.
    - by default, we'll just require that my password be passed with any requests to the server.
    - adding users can be done manually (probably use token auth like uelen does for Terra.)
5. easy to theme
    - single CSS file ideally.
6. pages stored in files and folders.
    - yeah, no need for a serious DB.

## big problem: multiple markups

how do we handle this problem: we have source format (executable lisp pages) and we ALSO want to be easily able to edit pages with the wysiwyg editor.

I am tempted to say that any page you want to edit with the wysiwyg editor must be markdown? but that feels like a bad solution.

The alternative is for the wiki to keep all the wiki pages around in source format, and additionally keep a compiled form around. Then, when you hit the `edit` button in the browser you get the markup.

OK, that's just going to be the way to go.

## compiling process

I had initially envisioned a simple process for this, with 3 steps, a pre-render, render, and post-render stage, each of which could be hooked up to executable scripts.

This means that we have, for example, a pre-render hook that runs a script to generate the internal link mapping, and then the render step could use that mapping for generating backlinks.

## backlinks

there are a few things we could do to handle backlinks.

- a persistent link database, javascript in each page to query the server, which queries the db.
    - imo, this is the most flexible.
- re-compile all the linked pages when a page is edited.
    - this is also probably not bad, but it does mean that we need the backlink assumption written into the project that manages this.
- recompile everything after any edit
    - I hate this, it's gonna be a huge bottleneck for big wikis (and even small ones tbh)
- don't worry about backlinks at all.
    - I continue to vascilate on this, because on the one hand, the places I've browsed with backlinks are SUPER cool, but on the other hand this adds a lot of complexity.
