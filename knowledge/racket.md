To use another language, use the `-I` flag

`racket -I sicp` is like `#lang sicp` in the repl
