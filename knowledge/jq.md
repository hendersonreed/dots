# to get a particular item

`cat file.json | jq .itemname`

# to read a string as json

example json:

```json
{
	"itemname": "{\"morejson\":\"super ugly and hard to read\"}"
}
```

`cat file.json | jq '.itemname | fromjson'`

will neatly format the json that's stored under the `itemname` key.
