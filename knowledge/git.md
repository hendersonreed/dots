# merging but ignoring a branch's changes

this is handy if you want to merge in old dev history but you want your current branch to be the "winner"

`git merge <any number of repo heads> --strategy ours`.

**note:**

this can be used with the remote copy of a local repo if you want to reset the remote with your local changes but not overwrite your history.

# pushing a tag

`git push <remote name, like origin> <tag name, like v0.11.2>`

# uncommitting changes that aren't in main branch:

`git reset --mixed $(git merge-base feature main)`

this will uncommit all the changes that are in `feature` and not in `main`. And then you can recommit as logical sets (using `git add --patch`)

# skipping githooks:

`git commit --no-verify`

# to show staged changes:

`git diff --cached`

# tricks for committing

## git commit --amend

For a long time I just thought this was about ammending commit messages. NO! it's more like a mini-rebase. It'll take whatever staged changes (you can also do `git commit -a --amend` to stage any changes and amend at the same time) and squash them into the previous commit. Then you just `git push -f` and you're good to go.

## git commit -av

Adds all changes to tracked files and opens up vim with a summary of the changes AND the -v command includes a diff in the file.

# to force my branch to be like another one:

git reset --hard {remote}/{branch}

then I'll need to force push to my branch.

# interactive staging or resetting

`git reset --patch` or `git reset -p`

`git add --patch` or `git add -p`

From the git documentation (here)[https://git-scm.com/docs/git-add]:

```
y - stage this hunk
n - do not stage this hunk
q - quit; do not stage this hunk or any of the remaining ones
a - stage this hunk and all later hunks in the file
d - do not stage this hunk or any of the later hunks in the file
g - select a hunk to go to
/ - search for a hunk matching the given regex
j - leave this hunk undecided, see next undecided hunk
J - leave this hunk undecided, see next hunk
k - leave this hunk undecided, see previous undecided hunk
K - leave this hunk undecided, see previous hunk
s - split the current hunk into smaller hunks
e - manually edit the current hunk
? - print help
```

# git rebase --ignore-whitespace

If there are a lot of whitespace changes, a rebase can get totally mangled.

this command ignores whitespace changes and works only on the text itself.
