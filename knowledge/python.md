# ipython in pytest tests

in the test, throw in 
```
from IPython import embed
embed()
```
to get an ipython repl right where you call `embed`. You can use this to pause a test and get an IPython repl inside.

When running the test you need to add "-s" to pytest, so that you can use stdin.

# piping into python

You can pipe into python and then use sys.stdin to access whatever you pipe in.

The below example pipes the file into python, which then loads it as json and prints it.

```bash
cat file.txt | python -c "import sys;import json;print(json.load(sys.stdin))"
```

# ipython

magic:

%autoreload 2, for having files reload when I edit them
%debug \<statement\> will run statement in pdb

# Repl-based development

* pdb++ as a drop-in replacement for pdb. Offers tab completion,
  replaces the 'pdb' module directly, so everything that has 'import pdb' now uses pdb++. On pip as `pdbpp`.
* ipdb - pdb but with highlighting, tab completion, lots more I think
* vim-slime
