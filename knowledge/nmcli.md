## vpn connection via cli

http://cweiske.de/tagebuch/networkmanager-vpn.htm

`nmcli con`

	lists all available connections

`nmcli con up "name of connection you want to use"`

connects to the given connection.
