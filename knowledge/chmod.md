# changing permissions to match another existing file

when you don't know exactly the right flags to turn on the right bits,
you can just use the `--reference` flag if you have an existing file with
the permissions you want to set. See example below:

`chmod --reference=referencefile.txt applyfile.txt`

The above command sets the permissions of applyfile to equal the permissions of
referencefile.
