from https://devhints.io/screen

- ctrl+a c 		Create a new screen tab inside a screen
- ctrl+a [0-9] 	Switch to a screen tab by number 0 through 9
- ctrl+a n 		Go to the next screen tab
- ctrl+a p 		Go to the previous screen tab
- ctrl+a k 		Kill current screen tab
- ctrl+a ctrl+d 	Detach the current screen and go back to the terminal (screen windows will stay running)
