# copying matching lines into a single buffer

`:g/match_pattern/yank A`

You'll want to empty the register beforehand

# emptying a register

if the register to be emptied is `a`, then run `:let @a=""` or `qaq`

# executing the current file

If the current file has 1. a shebang and 2. is executable, we can run `:! %:p` to execute it.

# reading the vim help

First, there's a whole set of handy intro help files - `:h user_01.txt` is the first one.

Secondly, there are links in the help:

- Press  CTRL-]  to jump to a subject under the cursor.
- Press  CTRL-O  to jump back (repeat to go further back).



# additional yank register

yank actually puts the yanked text into two registers - " (the same one as delete) and 0.

So, if you want to yank some text and then delete a line and then paste the original yanked text, you can `yy` and then any amount of operations later (provided none of the other operations were yanking), then simply run "0P to paste from the 0 register

source: https://unix.stackexchange.com/questions/26654/how-can-i-paste-overwriting-with-vim

# ale completion

with a language server enabled, ale will provide completions after you let the cursor sit for a few seconds. To select one of them, use ctrl-n to cycle through the list.

# search and replace with newlines

To replace a character with a newline using `:s/search/replace/g` or whatever, you need to use `\r` in place of `\n`.

# reading documentation

hitting `shift+k` when your cursor is over a word looks up that word using the `keywordprg` (default is `man`).

# deleting with regexes

`d/<word><cr>` deletes from the current cursor to the first match for word.

you can pair lots of things with / I think, including `v` for visual selection up to word, or c to delete it and enter insert mode.

? searches backwards, so we can use that here as well.

# Using the registers for pasting

`* <register> <action>`

		`* 2 p` will print the contents of buffer #2.

# Buffer operations

`:ls` lists the buffers

`:b<number of buffer to switch to>` switches buffers

`:bd` closes the currently open buffer

# netrw

`:Explore` to open it. Short is `:Ex`

`:Vexplore` to open in a vertical split. Short is `:Vex`

`:Sexplore` to open a horizontal split. Short is `:Sex`

`:Rexplore` to **reopen** it after having selected a file. `:Rex`

`:bd` to close it and return to the same file

# opening links

`gx` while on the link will use xdg-open I think.

# running a bash command from vim.

if you have a line that you want to run in vim, move the cursor to it, and hit

	:.!!

This will run the command and put the output into the vim buffer.

You can also run

	:exec '!'.getline('.')

to run a command in a separate buffer, which will close after the command is run and you hit enter.

Finally, you can run `:r! <command>`, which will run the command you type out and place the results in the current file. e.g. `:r! ls` will gather the filenames of all the files in your directory and print them directly into the file.
