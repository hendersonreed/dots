# brown noise waves:

`play -n synth brownnoise synth pinknoise mix synth sine amod 0.3 10`


# restart pipewire:

`systemctl --user restart pipewire`
