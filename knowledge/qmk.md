# `qmk flash`

Simply navigate to a keyboards folder and run `qmk flash`. This is more reliable than passing `-kb` and `-kb` flags to the command itself.
