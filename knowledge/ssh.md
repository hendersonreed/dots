# forwarding SSH key with ssh

`ssh -A` will carry my personal ssh key to the target machine so I can use it there for, ex, git or sshing to a third machine.
