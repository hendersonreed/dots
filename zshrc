# ~/.zshrc
fpath=($HOME/.zsh_completions $fpath)

# enable autocomplete functions
autoload -U compinit && compinit

source /etc/profile

# set vi mode
set -o vi

use_color=true

# zsh prompt things
autoload -U promptinit && promptinit
prompt walters

###########
# exports #
###########
export EDITOR=/usr/bin/nvim
export BROWSER=/usr/bin/firefox
export LS_COLORS="ow=01;34:di=4;36:ex=1;95"
export LESS="R"
export HISTFILE="$HOME/.zshhistory"
export SAVEHIST=500
export TERM="xterm"

NOTEDIR="$HOME/docs/linux_stuff/dotfiles/notes"
UMDIR="$HOME/docs/linux_stuff/dotfiles/knowledge"

##################
# custom aliases #
##################

# modifying standard commands.
alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias grep='grep --colour=auto'
alias lg="ls | grep"
alias ls='ls -hF --group-directories-first --color=auto'
alias more=less
alias python="python3"
alias vi="nvim" # I've gotten used to typing vi because I'm never sure if the system I'm on has vi or vim.
alias vim="nvim" # this way no matter what I do I get nvim

# my personal navigation/miscellaneous.
alias cl="clear && clear"
alias dots="cd ~/docs/linux_stuff/dotfiles/"
alias editalias="vim ~/.zshrc && source ~/.zshrc"
alias editnvim="vi ~/.config/init.lua"
alias notes="cd $NOTEDIR"


##################
# misc functions #
##################

# function for opening new terminals in the same directory 
# as the terminal the function is called in.
newt() {
    kitty --directory $(pwd) &> /dev/null &
    disown
}

opn() {
    xdg-open "$1" &> /dev/null &
    disown
}

########################
# note/todo functions: #
########################
note() {
	NOTENAME=${1:-$(date +%F)}
	vim "$NOTEDIR"/"$NOTENAME".md
}
notels() {
	ls "$NOTEDIR" | grep -v "[0-9]*-[0-9]*-[0-9]*" | cut -d "." -f 1
}
# note completion function
_note() {
	compadd $(notels)
}
compdef _note note


##################################################################
# personal markdown man pages experiment, stolen from hackernews #
##################################################################
mdless() {
      pandoc -s -f markdown -t man $1.md | groff -T utf8 -man | less
	}
umedit() {
	vim "$UMDIR"/$1.md;
}
um() { mdless "$UMDIR"/"$1"; }
umls() { ls "$UMDIR" | \
    grep ".*\.md" | \
	grep -v "[0-9]*-[0-9]*-0-9]*" | \
	cut -d "." -f 1 }
# completion functions for um
_um() { compadd $(umls) }
compdef _um um
compdef _um umedit

# for readline customization (zsh doesn't support readline)
bindkey "" history-incremental-search-backward
bindkey "[A" history-search-backward
bindkey "[B" history-search-forward

# set the amount of time it takes to switch vi modes.
export KEYTIMEOUT=1

# Set the prompt to “[user]@[host[ [vi mode] $ ”
PROMPT="%n@%m => "

function mychpwd {
    PATH=$(scratch-that-itch $PATH)
}
chpwd_functions+=(mychpwd)
